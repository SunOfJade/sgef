import firebase from 'firebase/app'
import 'firebase/firestore'
import credentials from './credentials'

const db = firebase.initializeApp(credentials).firestore()

 export default db;