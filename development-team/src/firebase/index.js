import firebase from 'firebase/app'
import 'firebase/firestore'

const FirebaseApp = firebase.initializeApp({ 
    apiKey: "AIzaSyAoWmed32NShzswdr7-6JFXcYTpzrwsBg4",
    authDomain: "fire-sge.firebaseapp.com",
    databaseURL: "https://fire-sge.firebaseio.com",
    projectId: "fire-sge",
    storageBucket: "fire-sge.appspot.com",
    messagingSenderId: "66466073892",
    appId: "1:66466073892:web:5a6027cfb2b8c7fc7077b1",
    measurementId: "G-8GPZ7B08E8"
 })

export default function install (Vue) {
    Vue.prototype.$firebase = FirebaseApp
}