import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    /* type and filters */
    userSex: ["Masculino", "Feminino"],
    typesOfUser: ["(Administrador", "Operador", "Usuario"],
    typesOfPhysicalSpace: ["Laboratório", "Sala de Aula", "Sala de Estudo"],
    typesOfBoard: ["Quadro Branco", "Quadro Negro"],
    typesOfEquipment: ["Projetor", "Televisão", "Caixa de Som"],

    user: {
      id: "user_1",
      email: "walmir.silva.santos00@aluno.ifce.edu.br",
      password: "123456",
      name: "Walmir Silva dos Santos",
      isAdmin: true
    },
    equipments: [
      {
        id: "equi_1",
        name: "Projetor Epson",
        description: "Projetor Epson 1000x",
        quantidade: 1
      },
      {
        id: "equi_2",
        name: "Projetor Multilaser",
        description: "Projetor Multilaser 1x",
        quantidade: 1
      },
      {
        id: "equi_3",
        name: "Mesa Digitalizadora Wacom",
        description: "Mesa Digitalizadora",
        quantidade: 1
      }
    ],
    physicalSpace: [
      {
        id: "phys_1",
        type: "Laboratório de Informática",
        name: "Lab. Informática 2",
        description: "Laboratório de Informática, Bloco Acadêmico - Térreo",
        capacity: 25,
        hasComputers: true,
        hasAirConditioning: true,
        hasProjector: true,
        board: "Quadro Branco",
        numberOfComputers: 25
      },
      {
        id: "phys_2",
        type: "Sala de Aula",
        name: "Sala 22",
        description: "Sala de Aula, Bloco Acadêmico - 1º Andar",
        capacity: 40,
        hasComputers: false,
        hasAirConditioning: true,
        hasProjector: false,
        board: "Quadro Branco"
      }
    ]
  },
  mutations: {},
  actions: {},
  modules: {}
});
