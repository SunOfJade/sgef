import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import(/* webpackChunkName: "home" */ "../views/Home.vue")
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue")
  },
  {
    path: "/recovery",
    name: "Recovery",
    component: () =>
      import(/* webpackChunkName: "recovery" */ "../views/Recovery.vue")
  },
  {
    path: "/new/user",
    name: "New User",
    component: () =>
      import(/* webpackChunkName: "new user" */ "../views/AddNewUser.vue")
  },
  {
    path: "/new/space",
    name: "New Space",
    component: () =>
      import(/* webpackChunkName: "new space" */ "../views/AddNewSpace.vue")
  },
  {
    path: "/new/equipment",
    name: "New Equipment",
    component: () =>
      import(
        /* webpackChunkName: "new equipment" */ "../views/AddNewEquipment.vue"
      )
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
